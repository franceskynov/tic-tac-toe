//
//  ViewController.swift
//  Tic-Tac-Toe
//
//  Created by Dev on 10/23/18.
//  Copyright © 2018 hightech-corp. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    
    @IBOutlet weak var playAgainButton: UIButton!
    @IBOutlet weak var wonLabel: UILabel!
    var gameIsActive = true
    var activePlayer = 1
    var gameState = [ 0, 0, 0, 0, 0, 0, 0, 0, 0]
    var winningConbinations = [
        [0, 1, 2],
        [3, 4, 5],
        [6, 7, 8],
        [0, 3, 6],
        [1, 4, 7],
        [2, 5, 8],
        [0, 4, 8],
        [2, 4, 6]
    ]
    @IBAction func changeStatus(_ sender: UIButton) {
        
        if gameState[sender.tag - 1] == 0 && gameIsActive == true{
            gameState[sender.tag - 1] = activePlayer
            if activePlayer == 1 {
                sender.setImage(UIImage(named: "Nought"), for: UIControlState())
                activePlayer = 2
            } else {
                sender.setImage(UIImage(named: "Cross"), for: UIControlState())
                activePlayer = 1
            }
        }
        
        for conbination in winningConbinations {
            if gameState[conbination[0]] != 0 && gameState[conbination[0]] == gameState[conbination[1]] && gameState[conbination[1]] == gameState[conbination[2]] {
                gameIsActive = false
                
                if gameState[conbination[0]] == 1 {
                    print("nought has won")
                   wonLabel.text = "Nought has won"
                } else {
                    print("cross has won")
                    wonLabel.text = "Cross has won"
                }
                wonLabel.isHidden = false
                playAgainButton.isHidden = false
            }
        }
        
        gameIsActive = false
        for i in gameState {
            if i == 0 {
                gameIsActive = true
            }
        }
        
        if gameIsActive == false {
            wonLabel.text = "It was a draw"
            wonLabel.isHidden = false
            playAgainButton.isHidden = false
        }
    }
    
    @IBAction func playAgain(_ sender: UIButton) {
        gameState = [ 0, 0, 0, 0, 0, 0, 0, 0, 0]
        gameIsActive = true
        activePlayer = 1
        playAgainButton.isHidden = true
        wonLabel.isHidden = true
        
        for i in 1...9 {
            let button = view.viewWithTag(i) as! UIButton
            button.setImage(nil, for: UIControlState())
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        wonLabel.isHidden = true
        playAgainButton.isHidden = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

